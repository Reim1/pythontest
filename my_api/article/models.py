from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class Cpuram(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)   
    cost = models.CharField(max_length=60)
    def __str__(self):
        return self.cost

class Normal(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    index = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    cost = models.CharField(max_length=60)
    buy = models.CharField(max_length=60)
    def __str__(self):
        return self.name

class Topic(models.Model):
    name = models.CharField(max_length=60)
    value =models.IntegerField(u'topic', validators=[MinValueValidator(1),MaxValueValidator(42)])
    def __str__(self):
        return self.name

class Vegtable(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    index = models.CharField(max_length=64,blank=True)
    name = models.CharField(max_length=64)
    sell = models.CharField(max_length=60)
    buy = models.CharField(max_length=60)
    type = models.CharField(max_length=40,default="")
    def __str__(self):
        return self.name
