from rest_framework import serializers

from .models import Cpuram,Normal,Topic,Vegtable

class Cpuramserializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    updated_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    class Meta:
        model = Cpuram
        fields = ('id','cost','created_at','updated_at')

class Normalserializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    updated_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    class Meta:
        model = Normal
        fields = ('id','index','created_at','updated_at','name','cost')

class Topicserializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Topic
        fields = ('value','name')

class Vegtableserializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    updated_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    class Meta:
        model = Vegtable
        fields = ('id','created_at','updated_at','name', 'sell','buy', 'type')

# 1.3.	cpu、内存使用率 
# 这个返回值是接口返回还是websocket返回，横坐标是半分钟为单位还是秒为单位,有返回样例吗
# 1.4.	Kafka topic消费状态
# 下拉框topic的值这个是固定写死的还是后台有接口返回值的
# 1.7.	cpu内存使用率查询
# 下拉框主机的值这个是固定写死的还是后台有接口返回值的
# 下拉框交易日期的值这个是固定写死的还是后台有接口返回值的
# 开始时间和结束时间是针对交易日期的吗,这个值是不是固定一天内的时间段例如9:00-15:00不能跨一天, 下方线图横坐标是写死1:00-24:00还是根据查询范围显示几点到几点,这个是接口返回吗,有返回样例吗
# 1.9.	告警信息查询
# 下拉框类型和模块的值这个是固定写死的还是后台有接口返回值的
