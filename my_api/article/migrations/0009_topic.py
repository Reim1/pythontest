# Generated by Django 4.0.6 on 2022-09-22 07:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0008_cpuram_created_at_cpuram_updated_at_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
                ('value', models.IntegerField(max_length=60)),
            ],
        ),
    ]
