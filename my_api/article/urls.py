from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
# router.register(r'man', views.ManViewSet)   #路由到ManViewSet视图
# router.register(r'api/kafk', views.list)   #路由到ManViewSet视图

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
# urlpatterns = [
#     path('', include(router.urls)), #使用router路由
#     path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
urlpatterns = [
    path('api/list/normal', views.Normals.as_view()),
    path('api/list/mine', views.CpuRam.as_view()),
    path('api/list/vegtable', views.Vegtables.as_view()),
    # path('api/list/extract', views.Topic.as_view())
]
