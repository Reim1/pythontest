from smtpd import DebuggingServer
from urllib import response
from rest_framework import viewsets

from .serializer import Cpuramserializer,Normalserializer,Topicserializer,Vegtableserializer
from .models import Cpuram,Normal,Vegtable
from django.http import JsonResponse
from django.core import serializers
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from utils.commonResponse import APIResponse

# class ManViewSet(viewsets.ModelViewSet):
#     queryset = Man.objects.all().order_by('name')  #查询结果给queryset
#     serializer_class = Manserializer     #对结果进序列化


class Normals(APIView):
    def get(self, request, *args, **kwargs):
        list = Normal.objects.all()
        serializer = Normalserializer(list, many=True)
        return APIResponse(results=serializer.data)
        
class CpuRam(APIView):
    def get(self, request, *args, **kwargs):
        list = Cpuram.objects.all()
        serializer = Cpuramserializer(list, many=True)
        return APIResponse(results=serializer.data)

class Vegtables(APIView):
    def get(self, request, *args, **kwargs):
        list = Vegtable.objects.all()
        serializer = Vegtableserializer(list, many=True)
        return APIResponse(results=serializer.data)
        
